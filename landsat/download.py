import urllib.request
import workerpool
import csv 
import re

"""
Takes a csv file (for landsat8 - downloaded from https://earthexplorer.usgs.gov/)
and downloads the true color pictures (from AWS api) on a chosen location
"""

url = "https://landsat-pds.s3.amazonaws.com/c1/L8/"
folder_path = ""
csv_file = ""

class DownloadPool(workerpool.Job):
    def __init__(self, url):
        self.url = url

    def run(self):
        try:
            image_name = self.url.split("/")[-1]
            save_to = folder_path + image_name
            urllib.request.urlretrieve(self.url, save_to)
            print(image_name + " saved.")

        except urllib.error.HTTPError as e:
            print('HTTP Error: ', e.code)

        except urllib.error.URLError as e:
            print('URL Error: ', e.reason)


# Open the downloaded csv, read the IDs and save the rgb links.
arr = []

with open(csv_file, newline='', encoding='latin-1') as f:
    reader = csv.DictReader(f) 
    for row in reader:
        id = row['Landsat Product Identifier']
        #print(id)

        # Regex for cutting part of the id
        m = re.search(r"_[0-9]{6}_",id)

        # Adding the cutted part + the id in the original url
        url = url + m.group(0)[1:4] + '/' + m.group(0)[4:-1] + '/' + id
        r = url + '/' + id + '_B2.TIF' 
        g = url + '/' + id + '_B3.TIF' 
        b = url + '/' + id + '_B4.TIF' 

        arr.append([r,g,b])
        
        # Reset the url to the original state
        url = "https://landsat-pds.s3.amazonaws.com/c1/L8/"


# Initialize a pool, 3 threads in this case
pool = workerpool.WorkerPool(size=3)

def download():
    for val in arr:
        for i in range(3):
            image = val[i] 
            job = DownloadPool(image)
            pool.put(job)



download()

# Send shutdown jobs to all threads, and wait until all the jobs have been completed
pool.shutdown()
pool.wait()
