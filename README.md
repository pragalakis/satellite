# Satellite Imagery

## download.py 

Downloads the true color pictures on a chosen map location from landsat satellite.

## rgb-composites.py

Combine the red green and blue bands in true color.
